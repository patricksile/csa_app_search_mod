const express = require('express');
const router = express.Router();

// Flat Data (FakeDataBase)
const { data } = require('../data/csaData.json');
const { profiles } = data;
const profilesLength = profiles.length;


// Serving the search.pug template to the / route
router.get('/', (req, res) => {

    res.render('search');
})

router.post('/', (req, res) => {

    const userInput = req.body.username.toLowerCase(); // username ofr the name='username' set in the form

    // Validation Logics

    // ID and Username validation
    if (/^[0-9]{9}$/.test(userInput) || /^[A-Z]?[a-z]+$/.test(userInput)) {

        // Collection of names in the database
        let nameList = [];
        
        for (let profileIndex = 0; profileIndex < profilesLength; profileIndex++) {
            
            
            if (profiles[profileIndex].ID === userInput) {

                nameList.push(profiles[profileIndex].name);
                break;
                

            } else if (profiles[profileIndex].name.toLowerCase().includes(userInput)) {

                nameList.push(profiles[profileIndex].name);

            }
        

        }

        if (nameList.length) {
            
            // Serving the noresult.pug template
            return res.render('search', { name: nameList });
        } else {
            
            // Returning the nameList on the search.pug templage
            return res.render('noresult', { name: userInput});
        }


        // Wrong input validation
    } else {

        return res.render('invalid');
    }

})

// Exporting router

module.exports = router;