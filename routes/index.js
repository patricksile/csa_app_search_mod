const express = require('express');
const router = express.Router();

// Serving the home.pug template to the root route
router.get('/', (req, res) => {

    res.render('home');
});

// Exporting this router file

module.exports = router;